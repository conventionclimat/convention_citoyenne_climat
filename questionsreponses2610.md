# Q1 Quels Etats ont été condamnés par la Commission Européenne pour Dépassement au dioxyde d'azote (condamnation datant d'il y a quelques jours)

En bref :
L’Allemagne, le Royaume-Uni, l’Italie, la Hongrie et la Roumanie sont aussi sous la menace d’une condamnation
En détail :
Dans un arrêt rendu jeudi 24 octobre, la Cour de justice européenne (CJUE) estime que la France a dépassé « de manière systématique et persistante la valeur limite annuelle pour le dioxyde d’azote (NO2) depuis le 1er janvier 2010 dans douze agglomérations et zones de qualité de l’air françaises ».
Le dioxyde d’azote (NO2), est un gaz très toxique émis principalement par le trafic routier, et en particulier par les véhicules diesels. Les douze agglomérations françaises concernées par ces dépassements répétés : Paris, Marseille, Lyon, Nice, Toulouse, Strasbourg, Montpellier, Grenoble, Reims, Clermont-Ferrand, Toulon, et la vallée de l’Arve.
La Commission européenne avait saisi la CJUE en mai 2018 sur les cas de la France et d’autres Etats.
L’Allemagne, le Royaume-Uni, l’Italie, la Hongrie et la Roumanie sont aussi sous la menace d’une condamnation pour des dépassements de NO2 ou de particules fines (PM10, de diamètre inférieur à 10 micromètres). Les juges de la CJUE n’ont pas encore rendu leur décision en ce qui concerne ces Etats.
Conclusion de l'ARRÊT DE LA COUR (septième chambre), 24 octobre 2019
"La Cour (septième chambre) déclare et arrête :
1) En dépassant de manière systématique et persistante la valeur limite annuelle pour le dioxyde d’azote (NO2) depuis le 1er janvier 2010 dans douze agglomérations et zones de qualité de l’air françaises, à savoir Marseille (FR03A02), Toulon (FR03A03), Paris (FR04A01), Auvergne-Clermont-Ferrand (FR07A01), Montpellier (FR08A01), Toulouse Midi-Pyrénées (FR12A01), zone urbaine régionale (ZUR) Reims Champagne-Ardenne (FR14N10), Grenoble Rhône-Alpes (FR15A01), Strasbourg (FR16A02), Lyon Rhône-Alpes (FR20A01), ZUR Vallée de l’Arve Rhône-Alpes (FR20N10) et Nice (FR24A01), et en dépassant de manière systématique et persistante la valeur limite horaire pour le NO2 depuis le 1er janvier 2010 dans deux agglomérations et zones de qualité de l’air, à savoir Paris (FR04A01) et Lyon Rhône-Alpes (FR20A01), la République française a continué de manquer, depuis cette date, aux obligations qui lui incombent en vertu de l’article 13, paragraphe 1, de la directive 2008/50/CE du Parlement européen et du Conseil, du 21 mai 2008, concernant la qualité de l’air ambiant et un air pur pour l’Europe, lu en combinaison avec l’annexe XI de cette directive, et ce depuis l’entrée en vigueur des valeurs limites en 2010.
La République française a manqué, depuis le 11 juin 2010, aux obligations qui lui incombent en vertu de l’article 23, paragraphe 1, de ladite directive, lu en combinaison avec l’annexe XV de celle-ci, et en particulier à l’obligation, établie à l’article 23, paragraphe 1, deuxième alinéa, de la même directive, de veiller à ce que la période de dépassement soit la plus courte possible."
2) La République française est condamnée aux dépens. »
Source : https://curia.europa.eu/jcms/jcms/p1_2524806/fr/"
 
#### Réponse Merci. Les autres pays sont « sous la menace » mais n’on pas été condamné (a la différence de la France) : est ce bien cela ?
#### C'est cela : la CJUE a été saisie pour leurs cas également, et la décision est attendue.

# Q2 Est-ce qu’on peut avoir la marge des constructeurs automobiles sur la vente d’une véhicule ? Avec les grandes catégories des véhicules

Le taux de marge pour l’industrie automobile en 2017 était de 29,5%. Le taux de marge est le ratio de l’excédent brut d’exploitation (= ce qui reste lorsqu’on a payé les salaires et les fournisseurs. L’excédent brut est partagé entre l’impôt sur les sociétés, la dépréciation des machines utilisées et la rémunération des actionnaires) sur la valeur totale produite par l’industrie.
Source : INSEE

# Q3 Est ce que le DPE est obligqtoire lors de la vente d’une habitation? Existe t il des execptions

Le DPE est obligatoire sauf pour les immeubles neufs ( de mémoire postérieurs à 2006) pour ceux là les objectifs d'isolation sont intégrés lors de la construction et le DPE est remis à la construction . Attention aussi aux autres locaux ( bureaux tertiaires, ateliers, caves transformées en souplex). Pas de sanction mais le délai de rétractation ne court pas tant que DPE pas remis : l'acquéreur peut refuser de signer 
Tout  est là 
https://www.ecologique-solidaire.gouv.fr/diagnostic-performance-energetique-dpe

# Q4 Dans l'empreinte carbone, on intègre les émissions territoriales, puis les importations. Mais les exports ?

Les exportations ne sont pas comptées.  Ou plutôt elles sont comptées du côté du pays importateur.
L’empreinte carbone (= émissions de ce qui est consommé en France)des Français est 55% due aux importations et 45% due à la production intérieure consommée en France. Pour ce qui est des émissions de la production en France, 75% est due à la demande intérieure et 25% aux exportations. En bout de course, on importe environ 400 MT d’équivalents CO2 et on exporte environ 125 MT d’équivalents CO2 par an.
Source : [MTES 2019](https://www.statistiques.developpement-durable.gouv.fr/sites/default/files/2019-05/datalab-46-chiffres-cles-du-climat-edition-2019-novembre2018.pdf)


# Q5 Il y a eu 3500 établissement contrôlés dans le cadre de l'évaluation de la Loi EGALIM pour vérifier l'adéquation avec les objectifs restauration collective bio et anti gaspillage. Quels ont été les établissements contrôlés et quels ont été les résultats ?

Reponse :

# Q6 Existe-t-il une loi sur l'obsolescence programmee ?

Oui : article l441-2 du code de la consommation : 
« Est interdite la pratique de l'obsolescence programmée qui se définit par le recours à des techniques par lesquelles le responsable de la mise sur le marché d'un produit vise à en réduire délibérément la durée de vie pour en augmenter le taux de remplacement. ». 
Le délit est puni de 2 ans d’emprisonnement, et 300 000 euros d’amende ou 5% du chiffre d’affaires (article L454-9). 
Les personnes physiques condamnées peuvent en plus être interdites d’exercer des fonctions publiques ou dans les entreprises. 
À notre connaissance, il n’y a pas encore eu de condamnation mais plusieurs enquêtes sont en cours (depuis 2017 pour l'une et 2018 pour l'autre, les deux dans le domaine informatique-électronique). 

Par ailleurs il est obligatoire depuis 2014 pour les vendeurs d’informer le consommateur sur la disponibilité des pièces détachées (loi Consommation). Une enquête menée en 2016 par 60 millions de consommateurs et les Amis de la Terre auprès de 500 points de vente rapportait un taux élevé de non disponibilité de ces informations 